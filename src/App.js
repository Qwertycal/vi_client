import React, { useState, useEffect } from 'react';
import './App.css';
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container';
import { Game, Header, NameDialog } from './components/';
import axios from 'axios';
import { Context } from './context';

function App() {
  const [name, setName] = useState('');
  const [board, setBoard] = useState();
  const [nameDialogStatus, setNameDialogStatus] = useState(true);
  const [movesCounter, setMovesCounter] = useState(0);
  const [gameTimer, setGameTimer] = useState(0);
  const [isGameStarted, setIsGameStarted] = useState(false);
  let timeout;

  useEffect(() => {
    if (isGameStarted) {

      timeout = setTimeout(() => {
        setGameTimer(gameTimer + 1);
      }, 1000);

      return () => {
        clearTimeout(timeout);
      };
    }
  }, [gameTimer]);

  const startGame = () => {
    setIsGameStarted(true);
    setGameTimer(1);
    axios.get('http://' + window.location.hostname + ':5000/api/puzzle/mixedPuzzle')
      .then((res) => {
        setBoard(res.data);
      })
  }

  const onSolve = (moves) => {
    setIsGameStarted(false);
    clearTimeout(timeout);
    let bodyParameters = {
      user_name: name,
      moves: moves,
      time: gameTimer
    }
    axios.post(
      'http://' + window.location.hostname + ':5000/api/puzzle/winResult',
      bodyParameters
    ).then((response) => {
      if (response.status === 201) {
        // 
      } else if (response.status === 200) {
        return
      }
      else if (response.status === 500) {
      }
    }).catch((error) => {
      // console.log('error: ',error)
    });

  }

  return (
    <React.Fragment>
      <CssBaseline />
      <Context.Provider value={{
        name, movesCounter, setMovesCounter, gameTimer, startGame
      }}>

        <Container fixed maxWidth="sm" className="container">
          <Header />
        </Container>

        <Container fixed maxWidth="sm" className="container">
          <NameDialog dialogStatus={nameDialogStatus} setNameDialogStatus={setNameDialogStatus} setPlayerName={setName} />
          <Game initialConfiguration={board} onSolveCallback={onSolve} />
        </Container>

      </Context.Provider>
    </React.Fragment>
  );
}

export default App;