import React, { useContext } from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import { Context } from './../../context';
import useStyles from './HeaderCSS';
import { WinnersBoardDialog } from './../'

function Header() {
  const classes = useStyles();

  const { name, movesCounter, gameTimer } = useContext(Context);
  return (
    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid item xs={3}>
          <Paper className={classes.paper}>Hello {name}</Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paper}> {movesCounter} steps</Paper>
        </Grid>
        <Grid item xs={3}>
          <Paper className={classes.paper}>{gameTimer} seconds</Paper>
        </Grid>
        <Grid item xs={3}>
          <WinnersBoardDialog />
        </Grid>
      </Grid>
    </div>
  )
}

export default Header