import React, { useEffect, useState, useContext } from 'react';
import './Game.css';
import { PuzzleItem, WinDialog } from './../';
import { Context } from './../../context';

function Game(props) {
    const { initialConfiguration, onSolveCallback } = props;
    const { movesCounter, setMovesCounter } = useContext(Context);
    const [squares, setSquares] = useState([]);
    const [winGameModalStatus, setWinGameModalStatus] = useState(false);
    const [previousGame, setPreviousGame] = useState([])

    useEffect(() => {
        if (squares === undefined || squares.length === 0 || previousGame !== initialConfiguration) {
            setSquares(initialConfiguration)
            setPreviousGame(initialConfiguration)
            setMovesCounter(0);
        }
    }, [props]);

    function checkWin(arr, n) {
        if (n === 1 || n === 0)
            return 1;
        if (arr[n - 1] < arr[n - 2])
            return 0;
        return checkWin(arr, n - 1);
    }

    function moveSquare(val) {
        let zeroIndex = squares.indexOf(0);
        let valIndex = squares.indexOf(val);

        if (valIndex + 4 === zeroIndex || valIndex - 4 === zeroIndex) {
            swap(valIndex, zeroIndex);
        } else if (valIndex + 1 === zeroIndex && zeroIndex % 4 !== 0) {
            swap(valIndex, zeroIndex);
        } else if (valIndex - 1 === zeroIndex && (zeroIndex + 1) % 4 !== 0) {
            swap(valIndex, zeroIndex)
        }
    }

    function swap(valIndex, zeroIndex) {
        let tempBoard = [...squares];
        tempBoard[zeroIndex] = squares[valIndex];
        tempBoard[valIndex] = 0;
        setSquares(() => [...tempBoard]);
        let arrToCheck = [...tempBoard];
        arrToCheck.splice(-1, 1);
        let winRes = checkWin(arrToCheck, tempBoard.length);
        setMovesCounter(movesCounter + 1);
        if (winRes === 1 && tempBoard[tempBoard.length - 1] === 0) {
            onSolveCallback(movesCounter + 1);
            setWinGameModalStatus(true);
        }
    }
    return (
        <>
            <WinDialog dialogStatus={winGameModalStatus} changeDialogStatus={setWinGameModalStatus} />
            <div className='board'>
                {squares && squares.map((puzzle, index) => (
                    <div key={index}>
                        <PuzzleItem value={puzzle} clickHandler={moveSquare} />
                    </div>
                ))}
            </div>
        </>
    )
}

export default Game;