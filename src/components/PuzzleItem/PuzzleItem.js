import React from 'react';
import './PuzzleItem.css';

function PuzzleItem(props) {
    return (
        <div className={props.value === 0 ? 'empty' : 'tiles'} onClick={() => props.clickHandler(props.value)}>
            {props.value}
        </div>
    )
}

export default PuzzleItem;