import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({    
    buton: {
      // padding: theme.spacing(1),
      height: '50px',
      textAlign: 'center',
      color: theme.palette.text.secondary,
    },
  }));

  export default useStyles;