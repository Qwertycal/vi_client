import React, {useState, useContext} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Context} from './../../context';

export default function NameDialog(props) {
  const {dialogStatus, setPlayerName, setNameDialogStatus} = props;
  const {startGame} = useContext(Context);

  const [errorStatus, setErrorStatus] = useState(false);
  const [name, setName] = useState('');

  const validate = () => {
    if (name.length > 0) {
      return true;      
    }else{
      return false
    }
  }

  const handleFormSubmit = (e) => {
    if(validate()){
      setErrorStatus(false);
      setPlayerName(name);
      setNameDialogStatus(false);
      startGame();
    }else{
      setErrorStatus(true);
    }
  }

  return (
    <div>     
       <Dialog open={dialogStatus}  aria-labelledby="form-dialog-title">
       <form onSubmit={handleFormSubmit} noValidate autoComplete="off">
        <DialogTitle id="form-dialog-title">Name</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label="name"
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value) }
            fullWidth
            error={errorStatus}
            helperText={errorStatus? "Insert name.": ''}
          />
        </DialogContent>
        <DialogActions>
  
          <Button onClick={() =>handleFormSubmit()} color="primary">
            Start new Game
          </Button>
        </DialogActions>
        </form>
      </Dialog>
    </div>
  );
}