import React, { useContext } from 'react';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Context } from './../../context';

export default function WinDialog(props) {
    const { dialogStatus, changeDialogStatus } = props;
    const { name, movesCounter, startGame, gameTimer } = useContext(Context);

    const handleStartNewGame = () => {
        changeDialogStatus(false);
        startGame()
    }

    return (
        <div>
            <Dialog open={dialogStatus} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">CONGRATULATIONS</DialogTitle>
                <DialogContent>
                    <Typography variant="caption" display="block" gutterBottom>
                        Well done {name}. 
                    </Typography>
                    <Typography variant="caption" display="block" gutterBottom>
                        You won this game in {movesCounter} moves and {gameTimer} seconds.
                    </Typography>
                    
                </DialogContent>
                <DialogActions>

                    <Button onClick={() =>handleStartNewGame()} color="primary">
                    {/* <Button color="primary">*/}
                        Start new Game
                    </Button> 
                </DialogActions>
            </Dialog>
        </div>
    );
}