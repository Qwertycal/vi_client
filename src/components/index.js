import Game from './Game/Game';
import Header from './Header/Header';
import PuzzleItem from './PuzzleItem/PuzzleItem';
import NameDialog from './NameDialog/NameDialog';
import WinDialog from './WinDialog/WinDialog';
import WinnersBoardDialog from './WinnersBoardDialog/WinnersBoardDialog';
import WinnersTable from './Table/Table';

export {Game, Header, PuzzleItem, NameDialog, WinDialog, WinnersBoardDialog, WinnersTable}